#pragma once

#include <unordered_set>

#include "FancyString.hpp"

struct StringSet
{
private:
    std::unordered_set<FancyString> strings_;

public:
    StringSet() = default;
    StringSet(std::initializer_list<FancyString> strings);
    StringSet(std::unordered_set<FancyString> strings);

    void add(FancyString const& str);

    int size() const;
    bool empty() const;
    bool contains(FancyString const& str) const;
};